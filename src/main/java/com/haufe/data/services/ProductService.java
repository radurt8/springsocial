package com.haufe.data.services;


import com.haufe.data.entities.Product;

public interface ProductService {
    Iterable<Product> listAllProducts();

    Iterable<Product> findByDescription(String description);

    Product getProductById(Integer id);

    Product saveProduct(Product product);

    void deleteProduct(Integer id);
}
