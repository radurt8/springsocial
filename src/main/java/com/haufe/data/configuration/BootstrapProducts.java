package com.haufe.data.configuration;

import com.haufe.data.entities.Product;
import com.haufe.data.repositories.ProductRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManagerFactory;
import java.math.BigDecimal;

@Component
public class BootstrapProducts  implements ApplicationListener<ContextRefreshedEvent> {

    private ProductRepository productRepository;

    private Logger log = Logger.getLogger(BootstrapProducts.class);

    @Autowired
    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        Product awesomeProduct = new Product();
        awesomeProduct.setDescription("awesomeProduct");
        awesomeProduct.setPrice(new BigDecimal("66.95"));
        awesomeProduct.setImageUrl("http://images.huffingtonpost.com/2012-07-01-HolyCrap.jpg");
        awesomeProduct.setProductId("235268845711068308");
        productRepository.save(awesomeProduct);
        log.info("Saved awesomeProduct - id: " + awesomeProduct.getId());

        Product anotherProduct = new Product();
        anotherProduct.setDescription("anotherProduct");
        anotherProduct.setImageUrl("http://www.vitamin-ha.com/wp-content/uploads/2012/11/Vh-Bling-Teeth-Funny-Products.jpg");
        anotherProduct.setProductId("168639393495335947");
        anotherProduct.setPrice(new BigDecimal("55.95"));
        productRepository.save(anotherProduct);
        log.info("Saved anotherProduct - id: " + anotherProduct.getId());

        log.info("Do your thing");
    }
}
