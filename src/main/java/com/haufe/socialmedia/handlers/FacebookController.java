package com.haufe.socialmedia.handlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.Post;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

@Controller
@RequestMapping("/")
public class FacebookController {

    private Facebook facebook;
    private ConnectionRepository connectionRepository;

    @Autowired
    public void setFacebook(Facebook facebook) {
        this.facebook = facebook;
    }

    @Autowired
    public void setConnectionRepository(ConnectionRepository connectionRepository) {
        this.connectionRepository = connectionRepository;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String helloFacebook(Model model) {
        if (connectionRepository.findPrimaryConnection(Facebook.class) == null || !facebook.isAuthorized()) {
            return "redirect:/connect/facebook";
        }
        model.addAttribute("facebookProfile", facebook.userOperations().getUserProfile());
        model.addAttribute("userPermissions", facebook.userOperations().getUserPermissions());
        return "hello";
    }

    @RequestMapping(value = "places", method = RequestMethod.GET)
    public String places(Model model) {
        if (connectionRepository.findPrimaryConnection(Facebook.class) == null || !facebook.isAuthorized()) {
            return "redirect:/connect/facebook";
        }
        model.addAttribute("facebookProfile", facebook.userOperations().getUserProfile());
        model.addAttribute("taggedPlaces", facebook.userOperations().getTaggedPlaces());
        return "facebook/places";
    }


    private BufferedImage getProfileImage() {
        byte[] profileImage = facebook.userOperations().getUserProfileImage();
        System.out.println(Arrays.toString(profileImage));
        InputStream in = new ByteArrayInputStream(profileImage);
        try {
            return ImageIO.read(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
